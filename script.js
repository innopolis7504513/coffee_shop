let burger = document.querySelector('#burger');
let nav = document.querySelector('#nav');

burger.addEventListener('click', () => {
  if (!nav.classList.contains('navigation')) {
    nav.classList.add('navigation');
  } else {
    nav.classList.remove('navigation');
  }
})

let submit = document.querySelector('#submit');
submit.addEventListener("click", (event) => {
  event.preventDefault();
  let success = document.querySelector('#success');
  let error = document.querySelector('#error');
  let inputName = document.querySelector('#name').value;
  let inputEmail = document.querySelector('#email').value;
  let inputMessage = document.querySelector('#message').value;
  if (inputName === "" || inputEmail === "" || inputMessage === "") {
    error.classList.add('form__fields-error_active');
  } else if (inputName !== "" & inputEmail !== "" & inputMessage !== "") {
      success.classList.add('wrapper-success_active');
      error.classList.remove('form__fields-error_active');
      inputName == ""; inputEmail == ""; inputMessage == "";
      setTimeout(() => success.classList.remove('wrapper-success_active'), 3000);
    }
});

